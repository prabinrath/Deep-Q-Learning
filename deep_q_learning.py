import numpy as np
import torch
import torch.optim as optim
import torch.nn as nn
from get_env_and_learner import GetEnvAndLearner
from dqn_utils import ReplayMemory
from copy import deepcopy
from collections import deque
import matplotlib.pyplot as plt
import random
import wandb
from constants import constants
wandb.init(project='deep-q-learning', entity='deep-rl-exp')

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(device)

env_folder = 'Breakout'
environment = env_folder+'Deterministic-v4'
const = constants(environment)

# Constant Parameters
GAMMA = const.GAMMA # Discount factor
POLICY_UPDATE_INTERVAL = const.POLICY_UPDATE_INTERVAL # Interval for policy update
TARGET_UPDATE_INTERVAL = const.TARGET_UPDATE_INTERVAL # Interval for target update
LR = const.LR # Adam learning rate
EPSILON_START = const.EPSILON_START # Annealing start
EPSILON_END = const.EPSILON_END # Annealing end
EXPLORATION_FRAMES = const.EXPLORATION_FRAMES # Annealing frames
BATCH_SIZE = const.BATCH_SIZE # Sampling size from memory
MEMORY_BUFFER = const.MEMORY_BUFFER # Replay buffer size
EPISODES = const.EPISODES # Number of episodes for training
VALIDATE_FREQ = const.VALIDATE_FREQ # Episodes

# environment, training policy, target policy
env, policy, target = GetEnvAndLearner(name = environment, learner='dqn')
target.eval()
renv = deepcopy(env)
loss_fn = nn.SmoothL1Loss()
optimizer = optim.Adam(policy.parameters(), lr=LR)

# Memory for Experience Replay
memory = ReplayMemory(MEMORY_BUFFER)
wandb.config = {
  "type": "DQN",
  "environment":environment,
  "learning_rate": LR,
  "memory-buffer": MEMORY_BUFFER,
  "explore": EXPLORATION_FRAMES,
  "batch_size": BATCH_SIZE
}
glob_frame = 0

def get_epsilon():
    # Piecewise Linear Annealing
    if glob_frame < EXPLORATION_FRAMES:
        return EPSILON_END + (EXPLORATION_FRAMES-glob_frame)*(EPSILON_START-EPSILON_END)/EXPLORATION_FRAMES
    elif glob_frame < 2*EXPLORATION_FRAMES:
        return EPSILON_END/10 + (2*EXPLORATION_FRAMES-glob_frame)*(EPSILON_END-EPSILON_END/10)/EXPLORATION_FRAMES
    else:
        return EPSILON_END/10

def select_action(state, act_dim, eps=None):    
    if eps==None:
        eps = get_epsilon()
    # Epsilon-greedy exploration
    if np.random.uniform() < eps:
        return np.random.choice(act_dim)
    else:
        with torch.no_grad():
            q_sa = policy(torch.tensor(state, device=device, dtype=torch.float))
        return torch.argmax(q_sa[0]).item()

def optimize_policy(samples):
    states, actions, rewards, next_states, terminals = zip(*samples)
    states = torch.tensor(np.vstack(states), device=device, dtype=torch.float)
    actions = torch.tensor(np.vstack(actions), device=device)
    rewards = torch.tensor(np.vstack(rewards), device=device, dtype=torch.float)
    next_states = torch.tensor(np.vstack(next_states), device=device, dtype=torch.float)
    terminals = torch.tensor(np.vstack(terminals), device=device, dtype=torch.float)

    q_sa = policy(states).gather(1, actions).squeeze()     
    with torch.no_grad(): 
        q_nsa_max = target(next_states).max(1).values
        q_sa_target = rewards.squeeze() + GAMMA * q_nsa_max * (1.0 - terminals.squeeze())

    # Optimize on the TD loss
    loss = loss_fn(q_sa, q_sa_target)
    optimizer.zero_grad()
    loss.backward()
    for param in policy.parameters():
        param.grad.data.clamp_(-1, 1)
    optimizer.step()            

def validate_policy():    
    renv.reset()
    done = False
    valid_reward = 0
    # Random FIRE to start episode
    for _ in range(random.randint(1,10)):
        _ = renv.get_state()
        _, _, _, _ = renv.step(1)  
    while not done:       
        state = renv.get_state()
        action = select_action(state, renv.act_dim, EPSILON_END/10)
        _, reward, done, _ = renv.step(action)
        valid_reward+=reward
    return valid_reward

def save_stats(train_reward_history, valid_reward_history, padding=10):
    reward_history = np.array(train_reward_history)
    smooth_reward_history = np.convolve(reward_history, np.ones(padding*2)/(padding*2), mode='valid')
    plt.plot(reward_history, label='Reward')
    plt.plot(smooth_reward_history, label='Smooth Reward')
    plt.xlabel('Episode')
    plt.ylabel('Reward')
    plt.legend(loc='upper left')
    plt.title('Deep Q-Learning')
    # plt.show()
    plt.savefig(env_folder+'_res_train_dqn.png')
    plt.clf()
    reward_history = np.array(valid_reward_history)
    smooth_reward_history = np.convolve(reward_history, np.ones(padding*2)/(padding*2), mode='valid')
    plt.plot(reward_history, label='Reward')
    plt.plot(smooth_reward_history, label='Smooth Reward')
    plt.xlabel('Checkpoint Episode')
    plt.ylabel('Reward')
    plt.legend(loc='upper left')
    plt.title('Deep Q-Learning')
    # plt.show()
    plt.savefig(env_folder+'_res_valid_dqn.png')
    plt.clf()

max_possible_reward = const.max_possible_reward
reward_increment = const.reward_increment
max_valid_reward = const.max_valid_reward
max_reward_target = max_valid_reward + reward_increment
train_reward_history = []
valid_reward_history = []
recent_train_reward = deque(maxlen=100)
recent_valid_reward = deque(maxlen=10)

for episode in range(EPISODES):
    # Default max episode steps is defined in Gym environments
    done = False
    episode_reward = 0
    
    # Random FIRE to start episode
    for _ in range(random.randint(1,10)):
        _ = env.get_state()
        _, _, _, _ = env.step(1)  
    
    while not done:       
        state = env.get_state()
        action = select_action(state, env.act_dim)
        next_state, reward, done, terminal_life_lost = env.step(action)  
        episode_reward+=reward      
        glob_frame+=1
        
        clipped_reward = reward if reward<1.0 else 1.0
        memory.push((state, action, clipped_reward, next_state, float(terminal_life_lost)))
        if memory.length()<MEMORY_BUFFER*0.05:
            glob_frame-=1 # Prevents epsilon decay for initial few steps
            continue
        else:
            if glob_frame%POLICY_UPDATE_INTERVAL==0:
                optimize_policy(memory.sample(BATCH_SIZE))

        if glob_frame%TARGET_UPDATE_INTERVAL==0:
            target.load_state_dict(policy.state_dict())

    wandb.log({'reward': episode_reward})
    # wandb.watch(policy)

    train_reward_history.append(episode_reward)
    recent_train_reward.append(episode_reward)
    avg_train_reward = round(np.mean(recent_train_reward),3)

    if episode%VALIDATE_FREQ==0:
        valid_reward = validate_policy()    
        max_valid_reward = max(valid_reward,max_valid_reward)
        valid_reward_history.append(valid_reward)
        recent_valid_reward.append(valid_reward)
        avg_valid_reward = round(np.mean(recent_valid_reward),3)
        save_stats(train_reward_history, valid_reward_history)
        print('Valid Reward:', valid_reward, ' | Avg Valid Reward:', avg_valid_reward)
        
        # Save model when there is a performance improvement
        if max_valid_reward>=max_reward_target:
            max_reward_target = min(max_possible_reward, max(max_reward_target,max_valid_reward)+reward_increment)-1        
            print('Episode: ', episode, ' | Max Validation Reward: ', max_valid_reward, ' | Epsilon: ', get_epsilon())
            torch.save(policy.state_dict(), 'Checkpoints/'+env_folder+'/'+environment+'(dqn'+str(int(max_valid_reward))+')'+'.dqn')
            max_valid_reward*=0.8
            if avg_valid_reward>=max_possible_reward:
                print('Best Model Achieved !!!')
                break

    print('Episode: ', episode, ' | Epsilon: ', round(get_epsilon(),3) , ' | Train Reward:', episode_reward, ' | Avg Train Reward:', avg_train_reward)

save_stats(train_reward_history, valid_reward_history)